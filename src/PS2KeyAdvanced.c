#include <eZ80.h>
#include <gpio.h>
#include <string.h>

#include "PS2KeyAdvanced.h"
// Internal headers for library defines/codes/etc
#include "PS2KeyCode.h"
#include "PS2KeyTable.h"
#include "timer.h"

/* Constant control functions to flags array
   in translated key code value order  */
const uint8_t control_flags[] = 
                {
                    _SHIFT, _SHIFT, _CTRL, _CTRL,
                    _ALT, _ALT_GR, _GUI, _GUI
                };

typedef struct Ps2_data
{
    // Private Variables
    volatile uint8_t    ps2mode;                    /* ps2mode contains
                                                        PS2_BUSY      bit 7 = busy until all expected bytes RX/TX
                                                        TX_MODE       bit 6 = direction 1 = TX, 0 = RX (default)
                                                        BREAK_KEY     bit 5 = break code detected
                                                        WAIT_RESPONSE bit 4 = expecting data response
                                                        E0_MODE       bit 3 = in E0 mode
                                                        E1_MODE       bit 2 = in E1 mode
                                                        LAST_VALID    bit 1 = last sent valid in case we receive resend
                                                                              and not sent anything 
                                                    */
    /* volatile RX buffers and variables accessed via interrupt functions */
    volatile uint16_t   rx_buffer[_RX_BUFFER_SIZE]; // buffer for data from keyboard
    volatile uint8_t    head;                       // _head = last byte written
             uint8_t    tail;                       // _tail = last byte read (not modified in IRQ ever)
    volatile int8_t     bytes_expected;
    volatile uint8_t    bitcount;                   // Main state variable and bit count for interrupts
    volatile uint8_t    shiftdata;
    volatile uint8_t    parity;

    // /* TX variables */
    volatile uint8_t    tx_buff[_TX_BUFFER_SIZE];   // buffer for keyboard commands
    volatile uint8_t    tx_head;                    // buffer write pointer
    volatile uint8_t    tx_tail;                    // buffer read pointer
    volatile uint8_t    last_sent;                  // last byte if resend requested
    volatile uint8_t    now_send;                   // immediate byte to send
    volatile uint8_t    response_count;             // bytes expected in reply to next TX
    volatile uint8_t    tx_ready;                   // TX status for type of send contains

    /* Output key buffering */
             uint16_t   key_buffer[_KEY_BUFF_SIZE]; // Output Buffer for translated keys
             uint8_t    key_head;                   // Output buffer WR pointer
             uint8_t    key_tail;                   // Output buffer RD pointer
             uint8_t    mode;                       // Mode for output buffer contains
                                                        /* _NO_REPEATS 0x80 No repeat make codes for _CTRL, _ALT, _SHIFT, _GUI
                                                           _NO_BREAKS  0x08 No break codes */

    // Key decoding variables
             uint8_t    PS2_led_lock;               // LED and Lock status
             uint8_t    PS2_lockstate[4];           // Save if had break on key for locks
             uint8_t    PS2_keystatus;              // current CAPS etc status for top byte
} ps2_data;


// Private function declarations
void    ps2_send_bit(void);
void    ps2_send_now(uint8_t);
int16_t ps2_send_next(void);
void    ps2_reset(void);
uint8_t ps2_decode_key(uint8_t);
void    ps2_set_lock(void);

ps2_data    g_ps2_data;
uint8_t     g_portC_OutputBits  = 0xFF;

/* ************************************************************************* */

void * set_vector(unsigned int vector,void (*hndlr)(void));

void digitalWriteClock(uint8_t val)
{
    if (val)
        g_portC_OutputBits = g_portC_OutputBits | 0x80;
    else
        g_portC_OutputBits = g_portC_OutputBits & 0x7F;

    PC_DR = g_portC_OutputBits;
} /* end digitalWriteClock */


void digitalWriteData(uint8_t val)
{
    if (val)
        g_portC_OutputBits = g_portC_OutputBits | 0x40;
    else
        g_portC_OutputBits = g_portC_OutputBits & 0xBF;

    PC_DR = g_portC_OutputBits;
} /* end digitalWriteData */


/* ************************************************************************* */
/* Interrupt handling */

void interrupt pc7_isr_inactive(void)
{
	PC_DR = 0x80;            /* clear interrupt flag */
} /* end pc7_isr_inactive */


void interrupt pc7_isr_active(void)
{
    if (g_ps2_data.ps2mode & _TX_MODE)
        ps2_send_bit();
    else
    {
        uint8_t         val;
        uint8_t         ret;
        static uint32_t prev_ms = 0;
        uint32_t        now_ms;

        // val = digitalRead(PS2_DataPin);
        val = PC_DR;
        val = val & 0x40;
        if (val == 0)
            val = 0;
        else
            val = 1;

        /* timeout catch for glitches reset everything */
        now_ms = millis();
        if (now_ms - prev_ms > 250)
        {
            g_ps2_data.bitcount = 0;
            g_ps2_data.shiftdata = 0;
        }
        prev_ms = now_ms;

        g_ps2_data.bitcount++;             // Now point to next bit
        switch (g_ps2_data.bitcount)
        {
            case 1: 
                // Start bit
                g_ps2_data.parity = 0;
                g_ps2_data.ps2mode |= _PS2_BUSY;    // set busy
                break;
            case 2:
            case 3:
            case 4:
            case 5:
            case 6:
            case 7:
            case 8:
            case 9: 
                // Data bits
                g_ps2_data.parity += val;                                   // another one received ?
                g_ps2_data.shiftdata >>= 1;                                 // right _SHIFT one place for next bit
                g_ps2_data.shiftdata |= (val) ? 0x80 : 0;                   // or in MSbit
                break;
            case 10: 
                // Parity check
                g_ps2_data.parity &= 1;                                     // Get LSB if 1 = odd number of 1's so parity bit should be 0
                if (g_ps2_data.parity == val)                               // Both same parity error
                    g_ps2_data.parity = 0xFD;                               // To ensure at next bit count clear and discard
                break;
            case 11: 
                // Stop bit lots of spare time now
                if (g_ps2_data.parity >= 0xFD)                              // had parity error
                {
                    ps2_send_now(PS2_KC_RESEND);                            // request resend
                    g_ps2_data.tx_ready |= _HANDSHAKE;
                }
                else                                                        // Good so save byte in _rx_buffer
                {
                    // Check _SHIFTed data for commands and action
                    ret = ps2_decode_key(g_ps2_data.shiftdata);
                    if (ret & 0x2)                                          // decrement expected bytes
                        g_ps2_data.bytes_expected--;

                    if ((g_ps2_data.bytes_expected <= 0) || (ret & 4))      // Save value ??
                    {
                        val = g_ps2_data.head + 1;
                        if (val >= _RX_BUFFER_SIZE)
                            val = 0;
                        if (val != g_ps2_data.tail)
                        {
                            // get last byte to save
                            g_ps2_data.rx_buffer[val] = (uint16_t)(g_ps2_data.shiftdata);
                            // save extra details
                            g_ps2_data.rx_buffer[val] |= (uint16_t)(g_ps2_data.ps2mode) << 8;
                            g_ps2_data.head = val;
                        } /* end if */
                    }
                    if (ret & 0x10)                                         // Special command to send (ECHO/RESEND)
                    {
                        ps2_send_now(g_ps2_data.now_send);
                        g_ps2_data.tx_ready |= _HANDSHAKE;
                    }
                    else
                        if (g_ps2_data.bytes_expected <= 0)                 // Receive data finished
                        {
                            // Set mode and status for next receive byte
                            g_ps2_data.ps2mode &= ~(_E0_MODE + _E1_MODE + _WAIT_RESPONSE + _BREAK_KEY);
                            g_ps2_data.bytes_expected = 0;
                            g_ps2_data.ps2mode &= ~_PS2_BUSY;
                            ps2_send_next();                                // Check for more to send
                        } /* end if */
                } /* end if */
                g_ps2_data.bitcount = 0;	                                // end of byte
                break;
            default: 
                // in case of weird error and end of byte reception re-sync
                g_ps2_data.bitcount = 0;
                break;
        } /* end switch */
    } /* end if */

	PC_DR = 0x80;                                                       // clear interrupt flag (bit 7)
} /* end pc7_isr_active */


void ps2_keyboard_detachInterrupt(void)
{
	/* set port C 7 interrupt vector */
	set_vector(PC7_IVECT, pc7_isr_inactive);

    /* Set the Port C bit 6 & 7 to input mode */
    setmode_PortC(PORTPIN_SEVEN|PORTPIN_SIX, GPIOMODE_INPUT);
} /* end ps2_keyboard_detachInterrupt */


void ps2_keyboard_attachInterrupt(void)
{  
	/* set port C 7 interrupt vector */
	set_vector(PC7_IVECT, pc7_isr_active);

	/* Set the Port C bit 7 falling edge triggered interrupt mode */
	setmode_PortC(PORTPIN_SEVEN, GPIOMODE_INTERRUPTFALLEDGE);

    /* Set the Port C bit 6 to input mode */
    setmode_PortC(PORTPIN_SIX, GPIOMODE_INPUT);
} /* end ps2_keyboard_attachInterrupt */


void ps2_calibration(void)
{
    setmode_PortC(PORTPIN_SEVEN, GPIOMODE_OUTPUT);

    while(1)
    {
        tenMicroSeconds();
        digitalWriteClock(1);
        // PC_DR = 0x80;
        sixtyMicroSeconds();
        digitalWriteClock(0);
        // PC_DR = 0x00;
    } /* end while */

 	setmode_PortC(PORTPIN_SEVEN, GPIOMODE_INPUT);

} /* end ps2_calibration */
/* PS/2 Decoding */

/* ************************************************************************* */
/* Decode value received to check for errors commands and responses
   NOT keycode translate yet
   returns  bit Or'ing
            0x10 send command in _now_send (after any saves and decrements)
            0x08 error abort reception and reset status and queues
            0x04 save value ( complete after translation )
            0x02 decrement count of bytes to expected

   Codes like EE, AA and FC ( Echo, BAT pass and fail) treated as valid codes 
   return code 6                                                             */
uint8_t ps2_decode_key(uint8_t value)
{
    uint8_t state;

    state = 6;             // default state save and decrement

    // Anything but resend received clear valid value to resend
    if (value != PS2_KC_RESEND)
        g_ps2_data.ps2mode &= ~(_LAST_VALID);

    // First check not a valid response code from a host command
    if (g_ps2_data.ps2mode & _WAIT_RESPONSE)
        if (value < 0xF0)
            return state;      // Save response and decrement

    // E1 Pause mode  special case just decrement
    if (g_ps2_data.ps2mode & _E1_MODE)
        return 2;

    switch(value)
    {
        case 0:                                     // Buffer overrun Errors Reset modes and buffers
        case PS2_KC_OVERRUN:
            ps2_reset();
            state = 0xC;
            break;
        case PS2_KC_RESEND:                         // Resend last byte if we have sent something
            if (g_ps2_data.ps2mode & _LAST_VALID)
            {
                g_ps2_data.now_send = g_ps2_data.last_sent;
                state = 0x10;
            }
            else
                state = 0;
            break;
        case PS2_KC_ERROR:                          // General error pass up but stop any sending or receiving
            g_ps2_data.bytes_expected = 0;
            g_ps2_data.ps2mode        = 0;
            g_ps2_data.tx_ready       = 0;
            state = 0xE;
            break;
        case PS2_KC_KEYBREAK:                       // break Code - wait the final key byte
            g_ps2_data.bytes_expected  = 1;
            g_ps2_data.ps2mode        |= _BREAK_KEY;
            state                      = 0;
            break;
        case PS2_KC_ECHO:                           // Echo if we did not originate echo back
            state = 4;                              // always save
            if ((g_ps2_data.ps2mode & _LAST_VALID) && (g_ps2_data.last_sent != PS2_KC_ECHO))
            {
                g_ps2_data.now_send  = PS2_KC_ECHO;
                state               |= 0x10;        // send _command on exit
            } /* end if */
            break;
        case PS2_KC_BAT:                            // BAT pass
            g_ps2_data.bytes_expected = 0;          // reset as if in middle of something lost now
            state                     = 4;
            break;
        case PS2_KC_EXTEND1:                        // Major extend code (PAUSE key only)
            if ( !(g_ps2_data.ps2mode & _E1_MODE))  // First E1 only
            {
                g_ps2_data.bytes_expected = 7;      // seven more bytes
                g_ps2_data.ps2mode |= _E1_MODE;
                g_ps2_data.ps2mode &= ~_BREAK_KEY;  // Always a make
            } /* end if */
            state = 0;
            break;
        case PS2_KC_EXTEND:                         // Two byte Extend code
            g_ps2_data.bytes_expected  = 1;         // one more byte at least to wait for
            g_ps2_data.ps2mode        |= _E0_MODE;
            state                      = 0;
            break;
        default:
            break;
    } /* end switch */
    return state;
} /* end ps2_decode_key */


/* ************************************************************************* */
/* Send Code */

/* Send data to keyboard
   Data pin direction should already be changed
   Start bit would be already set so each clock setup for next clock
   parity and _bitcount should be 0 already and busy should be set

   Start bit setting is due to bug in attachinterrupt not clearing pending interrupts
   Also no clear pending interrupt function   */
void ps2_send_bit(void)
{
    uint8_t val = 0;

    g_ps2_data.bitcount++;                                      // Now point to next bit
    switch (g_ps2_data.bitcount)
    {
        case 1: 
        // #if defined( PS2_CLEAR_PENDING_IRQ ) 
        //         // Start bit due to Arduino bug
        //         digitalWrite( PS2_DataPin, LOW );
        //         break;
        // #endif
        case 2:
        case 3:
        case 4:
        case 5:
        case 6:
        case 7:
        case 8:
        case 9:
            // Data bits
            val = g_ps2_data.shiftdata & 0x01;                  // get LSB
            // digitalWrite(PS2_DataPin, val); // send start bit
            digitalWriteData(val);                              // send start bit
            g_ps2_data.parity += val;                           // another one received ?
            g_ps2_data.shiftdata >>= 1;                         // right _SHIFT one place for next bit
            break;
        case 10:
            // Parity - Send LSB if 1 = odd number of 1's so parity should be 0
            // digitalWrite(PS2_DataPin, (~_parity & 1));
            digitalWriteData(~g_ps2_data.parity & 1);           // send start bit
            break;
        case 11: 
            // Stop bit write change to input pull up for high stop bit
            // pininput(PS2_DataPin);
        	setmode_PortC(PORTPIN_SIX, GPIOMODE_INPUT);
            break;
        case 12: 
            // Acknowledge bit low we cannot do anything if high instead of low
            if ( !((g_ps2_data.now_send == PS2_KC_ECHO) || (g_ps2_data.now_send == PS2_KC_RESEND)) )
            {
                g_ps2_data.last_sent = g_ps2_data.now_send;     // save in case of resend request
                g_ps2_data.ps2mode |= _LAST_VALID;
            } /* end if */
            // clear modes to receive again
            g_ps2_data.ps2mode &= ~_TX_MODE;
            if (g_ps2_data.tx_ready & _HANDSHAKE)               // If _HANDSHAKE done
                g_ps2_data.tx_ready &= ~_HANDSHAKE;
            else                              
                // else we finished a command
                g_ps2_data.tx_ready &= ~_COMMAND;
            if ( !(g_ps2_data.ps2mode & _WAIT_RESPONSE))        //  if not wait response
                ps2_send_next();                                // check anything else to queue up
            g_ps2_data.bitcount = 0;	                        // end of byte
            break;
        default: 
            // in case of weird error and end of byte reception re-sync
            g_ps2_data.bitcount = 0;
    } /* end switch */
} /* end ps2_send_bit */


/* Takes a byte sets up variables and starts the data sending processes
   Starts the actual byte transmission
   calling code must make sure line is idle and able to send
   Whilst this function adds long delays the process of the delays
   will STOP the interrupt source (keyboard) externally when clock held low
   _tx_ready contains 2 flags checked in this order
    _HANDSHAKE   command sent as part of receiving e.g. ECHO, RESEND
    _COMMAND     other commands not part of receiving
   Main difference _bytes_expected is NOT altered in _HANDSHAKE mode
   in command mode we update _bytes_expected with number of response bytes   */
void ps2_send_now(uint8_t command)
{
    g_ps2_data.shiftdata = command;
    g_ps2_data.now_send  = command;                             // copy for later to save in last sent
    // #if defined( PS2_CLEAR_PENDING_IRQ ) 
    // g_ps2_data.bitcount = 0;          // AVR/SAM ignore extra interrupt
    // #else
    g_ps2_data.bitcount = 1;          // Normal processors
    // #endif
    g_ps2_data.parity   = 0;
    g_ps2_data.ps2mode |= _TX_MODE + _PS2_BUSY;

    // Only do this if sending a command not from Handshaking
    if ( !(g_ps2_data.tx_ready & _HANDSHAKE) && (g_ps2_data.tx_ready & _COMMAND))
    {
        g_ps2_data.bytes_expected  = g_ps2_data.response_count; // How many bytes command will generate
        g_ps2_data.ps2mode        |= _WAIT_RESPONSE;
    } /* end if */

    // STOP interrupt handler 
    // Setting pin output low will cause interrupt before ready
    // detachInterrupt(digitalPinToInterrupt(PS2_IrqPin));
    ps2_keyboard_detachInterrupt();

    // set pins to outputs and high
    // digitalWrite(PS2_DataPin, HIGH);
    g_portC_OutputBits = 0xFF;
    // PC_DR = g_portC_OutputBits;
    // pinMode(PS2_DataPin, OUTPUT);

    // digitalWrite(PS2_IrqPin, HIGH);
    PC_DR = g_portC_OutputBits;
    // pinMode(PS2_IrqPin, OUTPUT);
    setmode_PortC(PORTPIN_SEVEN|PORTPIN_SIX, GPIOMODE_OUTPUT);

    // PC_DR = g_portC_OutputBits;

    // // Essential for PS2 spec compliance
    // delayMicroseconds(10);
    tenMicroSeconds();

    // set Clock LOW
    // digitalWrite(PS2_IrqPin, LOW);
    digitalWriteClock(0);

    // Essential for PS2 spec compliance
    // set clock low for 60us
    // delayMicroseconds(60);
    sixtyMicroSeconds();

    // Set data low - Start bit
    // digitalWrite(PS2_DataPin, LOW);
    digitalWriteData(0);

    // set clock to input_pullup data stays output while writing to keyboard
    // pininput(PS2_IrqPin);
	/* Set the Port C bit 7 falling edge triggered interrupt mode */
	setmode_PortC(PORTPIN_SEVEN, GPIOMODE_INTERRUPTFALLEDGE);

    // Restart interrupt handler
    // attachInterrupt(digitalPinToInterrupt( PS2_IrqPin ), ps2interrupt, FALLING);
	/* set port C bit 7 interrupt vector */
	set_vector(PC7_IVECT, pc7_isr_active);

    // //  wait clock interrupt to send data
} /* end ps2_send_now */



/* Send next byte/command from TX queue and start sending
   Must be ready to send and idle
   Assumes commands consist of 1 or more bytes and wait for response then may or
   not be followed by further bytes to send with or without response
    Checks
    1/ Buffer empty return empty buffer
    2/ Busy return busy (will be checked by interrupt routines later)
    3/ Read next byte (next byte to send)
    4/ Check if following byte(s) are command/data or response

    Returns  1 if started transmission or queued
            -134 if already busy
            -2 if buffer empty

    Note PS2_KEY_IGNORE is used to denote a byte(s) expected in response */
int16_t ps2_send_next(void)
{
    uint8_t i   = 0;
    int16_t val = -1;

    // Check buffer not empty
    i = g_ps2_data.tx_tail;
    if (i == g_ps2_data.tx_head)
        return -2;

    // set command bit in _tx_ready as another command to do
    g_ps2_data.tx_ready |= _COMMAND;

    // Already item waiting to be sent or sending interrupt routines will call back
    if (g_ps2_data.tx_ready & _HANDSHAKE)
        return -134;

    // if busy let interrupt catch and call us again
    if (g_ps2_data.ps2mode & _PS2_BUSY)
        return -134;

    // Following only accessed when not receiving or sending protocol bytes
    // Scan for command response and expected bytes to follow
    g_ps2_data.response_count = 0;
    do
    {
        i++;
        if (i >= _TX_BUFFER_SIZE)
            i = 0;
        if (val == -1)
            val = g_ps2_data.tx_buff[i];
        else
            if (g_ps2_data.tx_buff[i] != PS2_KEY_IGNORE)
                break;
            else
                g_ps2_data.response_count++;
            g_ps2_data.tx_tail = i;
    }
    while (i != g_ps2_data.tx_head);
        // Now know what to send and expect start the actual wire sending
        ps2_send_now(val);
    return 1;
} /* end ps2_send_next */


/*  Send a byte to the TX buffer
    Value in buffer of PS2_KEY_IGNORE signifies wait for response,
     Ruse one for each byte expected

    Returns -4 - if buffer full (buffer overrun not written)
    Returns 1 byte written when done */
int ps2_send_byte(uint8_t val)
{
    uint8_t ret;

    ret = g_ps2_data.tx_head + 1;
    if (ret >= _TX_BUFFER_SIZE)
        ret = 0;
    if (ret != g_ps2_data.tx_tail)
    {
        g_ps2_data.tx_buff[ret] = val;
        g_ps2_data.tx_head = ret;
        return 1;
    } /* end if */
    return -4;
} /* end ps2_send_byte */


/* Build command to send lock status
    Assumes data is within range */
void ps2_set_lock(void)
{
    ps2_send_byte(PS2_KC_LOCK);             // send command
    ps2_send_byte(PS2_KEY_IGNORE);          // wait ACK
    ps2_send_byte(g_ps2_data.PS2_led_lock); // send data from internal variable
    if (ps2_send_byte(PS2_KEY_IGNORE))      // wait ACK
        ps2_send_next();                    // if idle start transmission
} /* end ps2_set_lock */

/* ************************************************************************* */

/*  Send echo command to keyboard
    returned data in keyboard buffer read as keys                            */
void ps2_echo(void)
{
    ps2_send_byte( PS2_KC_ECHO);            // send command
    if ((ps2_send_byte(PS2_KEY_IGNORE)))    // wait data PS2_KC_ECHO
        ps2_send_next();                    // if idle start transmission
} /* end ps2_echo */


/*  Get the ID used in keyboard
    returned data in keyboard buffer read as keys                            */
void ps2_readID( void )
{
    ps2_send_byte(PS2_KC_READID);           // send command
    ps2_send_byte(PS2_KEY_IGNORE);          // wait ACK
    ps2_send_byte(PS2_KEY_IGNORE);          // wait data
    if (ps2_send_byte(PS2_KEY_IGNORE))      // wait data
        ps2_send_next();                    // if idle start transmission
} /* end ps2_readID */


/*  Get the current Scancode Set used in keyboard
    returned data in keyboard buffer read as keys                            */
void ps2_getScanCodeSet(void)
{
    ps2_send_byte(PS2_KC_SCANCODE);         // send command
    ps2_send_byte(PS2_KEY_IGNORE);          // wait ACK
    ps2_send_byte(0);                       // send data 0 = read
    ps2_send_byte(PS2_KEY_IGNORE);          // wait ACK
    if (ps2_send_byte(PS2_KEY_IGNORE))      // wait data
        ps2_send_next();                    // if idle start transmission
} /* end ps2_getScanCodeSet */


/* Returns the current status of Locks                                       */
uint8_t ps2_getLock(void)
{
    return g_ps2_data.PS2_led_lock;
} /* end ps2_getLock */


/* Sets the current status of Locks and LEDs                                 */
void ps2_setLock(uint8_t code)
{
    code &= 0xF;                            // To allow for rare keyboards with extra LED
    g_ps2_data.PS2_led_lock = code;         // update our lock copy
    g_ps2_data.PS2_keystatus &= ~_CAPS;     // Update copy of _CAPS lock as well
    g_ps2_data.PS2_keystatus |= (code & PS2_LOCK_CAPS ) ? _CAPS : 0;
    ps2_set_lock();
} /* end ps2_setLock */


void ps2_reset(void)
{
    /* reset buffers and states                                              */
    memset(&g_ps2_data, 0, sizeof(ps2_data));
    g_portC_OutputBits = 0xFF;
} /* end ps2_reset */


uint8_t ps2_key_available()
{
    int8_t  i   = 0;

    i = g_ps2_data.head - g_ps2_data.tail;
    if (i < 0)
        i += _RX_BUFFER_SIZE;
    return (uint8_t)i;
} /* end ps2_key_available */



/*  Translate PS2 keyboard code sequence into our key code data
    PAUSE key (_E1_MODE) is de_ALT with as special case, and
    command responses not translated

    Called from read function as too long for in interrupt

    Returns 0 for no valid key or processed internally ignored or similar
            0 for empty buffer                                               */
uint16_t ps2_translate(void)
{
    uint8_t     index   = 0;
    uint8_t     length  = 0;
    uint8_t     data    = 0;
    uint16_t    retdata = 0;

    // get next character
    // Check first something to fetch
    index = g_ps2_data.tail;
    // check for empty buffer
    if (index == g_ps2_data.head)
        return 0;

    index++;
    if (index >= _RX_BUFFER_SIZE)
        index = 0;
    
    g_ps2_data.tail = index;

    // Get the flags byte break modes etc in this order
    data  = g_ps2_data.rx_buffer[index] & 0xFF;
    index = (g_ps2_data.rx_buffer[index] & 0xFF00) >> 8;

    // Catch special case of PAUSE key
    if (index & _E1_MODE)
        return (PS2_KEY_PAUSE + _FUNCTION);

    // Ignore anything not actual keycode but command/response
    // Return untranslated as valid
    if (   ( (data >= PS2_KC_BAT) && (data != PS2_KC_LANG1) && (data != PS2_KC_LANG2) )
        || (index & _WAIT_RESPONSE) )
        return (uint16_t)data;

    // Gather the break of key status
    if (index & _BREAK_KEY)
        g_ps2_data.PS2_keystatus |= _BREAK;
    else
        g_ps2_data.PS2_keystatus &= ~_BREAK;

    retdata = 0;    // error code by default

    // Scan appropriate table
    if (index & _E0_MODE)
    {
        length = sizeof(extended_key) / sizeof(extended_key[0]);
        for (index = 0; index < length; index++)
            if (data == extended_key[index][0])
            {
                retdata = extended_key[ index ][ 1 ];
                break;
            } /* end if */
    }
    else
    {
        length = sizeof(single_key) / sizeof(single_key[0]);
        for (index = 0; index < length; index++)
            if (data == single_key[index][0])
            {
                retdata = single_key[index][1];
                break;
            } /* end if */
    } /* end if */

    // trap not found key
    if (index == length)
        retdata = 0;

    /* valid found values only */
    if (retdata > 0)
    {
        if (retdata <= PS2_KEY_CAPS)
        {   
            // process lock keys need second make to turn off
            if (g_ps2_data.PS2_keystatus & _BREAK)
            {
                g_ps2_data.PS2_lockstate[retdata] = 0; // Set received a break so next make toggles LOCK status
                retdata = PS2_KEY_IGNORE;     // ignore key
            }
            else
            {
                if (g_ps2_data.PS2_lockstate[retdata] == 1)
                    retdata = PS2_KEY_IGNORE;   // ignore key if make and not received break
                else
                {
                    g_ps2_data.PS2_lockstate[retdata] = 1;
                    switch(retdata)
                    {
                        case PS2_KEY_CAPS:   
                            index = PS2_LOCK_CAPS;
                            // Set CAPS lock if not set before
                            if (g_ps2_data.PS2_keystatus & _CAPS)
                                g_ps2_data.PS2_keystatus &= ~_CAPS;
                            else
                                g_ps2_data.PS2_keystatus |= _CAPS;
                            break;
                        case PS2_KEY_SCROLL: 
                            index = PS2_LOCK_SCROLL;
                            break;
                        case PS2_KEY_NUM:
                            index = PS2_LOCK_NUM;
                            break;
                    }
                    
                    // Now update PS2_led_lock status to match
                    if (g_ps2_data.PS2_led_lock & index)
                    {
                        g_ps2_data.PS2_led_lock &= ~index;
                        g_ps2_data.PS2_keystatus |= _BREAK;     // send as break
                    }
                    else
                        g_ps2_data.PS2_led_lock |= index;
                    ps2_set_lock( );
                }
            }
        }
        else
            if ((retdata >= PS2_KEY_L_SHIFT) && (retdata <= PS2_KEY_R_GUI))
            { 
                // Update bits for _SHIFT, _CTRL, _ALT, _ALT GR, _GUI in status
                index = control_flags[retdata - PS2_KEY_L_SHIFT];
                if (g_ps2_data.PS2_keystatus & _BREAK)
                    g_ps2_data.PS2_keystatus &= ~index;
                else
                    // if already set ignore repeats if flag set
                    if ((g_ps2_data.PS2_keystatus & index) && (g_ps2_data.mode & _NO_REPEATS))
                        retdata = PS2_KEY_IGNORE; // ignore repeat _SHIFT, _CTRL, _ALT, _GUI
                    else
                        g_ps2_data.PS2_keystatus |= index;
            }
            else
                // Numeric keypad ONLY works in numlock state or when _SHIFT status
                if ((retdata >= PS2_KEY_KP0) && (retdata <=  PS2_KEY_KP_DOT))
                    if ( !(g_ps2_data.PS2_led_lock & PS2_LOCK_NUM) || (g_ps2_data.PS2_keystatus & _SHIFT))
                        retdata = scroll_remap[ retdata - PS2_KEY_KP0 ];
                // Sort break code handling or ignore for all having processed the _SHIFT etc status
                if ((g_ps2_data.PS2_keystatus & _BREAK) && (g_ps2_data.mode & _NO_BREAKS))
                    return (uint16_t)PS2_KEY_IGNORE;
                // Assign Function keys _mode
                if (((retdata <= PS2_KEY_SPACE) || (retdata >= PS2_KEY_F1)) && (retdata != PS2_KEY_EUROPE2))
                    g_ps2_data.PS2_keystatus |= _FUNCTION;
                else
                    g_ps2_data.PS2_keystatus &= ~_FUNCTION;
    } /* end if */
    return (retdata | ((uint16_t)g_ps2_data.PS2_keystatus << 8));
} /* end ps2_translate */



/* ************************************************************************* */

/* ************************************************************************* */
/* Public functions */



/* Returns count of available processed key codes

   If processed key buffer (_key_buffer) buffer returns max count
   else processes input key code buffer until
     either input buffer empty
         or output buffer full
     returns actual count

   Returns   0 buffer empty
             1 to buffer size less 1 as 1 to full buffer

   As with other ring buffers here when pointers match
   buffer empty so cannot actually hold buffer size values                   */
uint8_t ps2_available(void)
{
    int8_t      i       = 0;
    int8_t      idx     = 0;
    uint16_t    data    = 0;

    // check output queue
    i = g_ps2_data.key_head - g_ps2_data.key_tail;
    if (i < 0)
        i += _KEY_BUFF_SIZE;
    
    while (i < (_KEY_BUFF_SIZE - 1)) // process if not full
        if (ps2_key_available())         // not check for more keys to process
        {
            data = ps2_translate();         // get next translated key
            if (data == 0)             // unless in buffer is empty
                break;
            if (   ((data & 0xFF) != PS2_KEY_IGNORE)
                && ((data & 0xFF) > 0))
            {
                idx = g_ps2_data.key_head + 1;         // point to next space
                if (idx >= _KEY_BUFF_SIZE)  // loop to front if necessary
                    idx = 0;
                g_ps2_data.key_buffer[idx] = data; // save the data to out buffer
                g_ps2_data.key_head = idx;
                i++;                      // update count
            } /* end if */
        }
        else
            break;                      // exit nothing coming in
    return (uint8_t)i;
} /* end ps2_available */

/* read a decoded key from the keyboard buffer
   returns 0 for empty buffer */   
uint16_t ps2_read(void)
{
    uint16_t    result  = 0;
    uint8_t     idx     = 0;

    if (result = ps2_available())
    {
        idx = g_ps2_data.key_tail;
        idx++;
        if (idx >= _KEY_BUFF_SIZE)  // loop to front if necessary
            idx = 0;
        g_ps2_data.key_tail = idx;
        result = g_ps2_data.key_buffer[idx];
    } /* end if */
    return result;
} /* end ps2_read */

void ps2_begin(void)
{
    ps2_reset();
    ps2_keyboard_attachInterrupt();
} /* end ps2_begin */