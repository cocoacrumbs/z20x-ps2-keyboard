#include <eZ80.h>

#define UART                 0			// change to 1 when using UART 1
#define UART_BPS             57600
#define UART_DATA_BITS       8
#define UART_PARITY          0
#define UART_STOP_BITS       1

#define	UART_FCTL	     	 UART0_FCTL
#define UART_RBR             UART0_RBR
#define UART_THR             UART0_THR
#define UART_BRG_L           UART0_BRG_L
#define UART_BRG_H           UART0_BRG_H
#define UART_LCTL            UART0_LCTL
#define UART_LSR             UART0_LSR

#define LCTL_DLAB            (unsigned char)0x80
#define LSR_THRE             (unsigned char)0x20
#define LSR_DR               (unsigned char)0x01

#define SetLCTL(d, p, s)     UART_LCTL = ((d-5)&3) | (((s-1)&1)<<2) | (p&3)


extern long SysClkFreq;


void uart0_init(void) 
{ 
    unsigned short int i;
    unsigned short     brg;

    brg = SysClkFreq/(16 * UART_BPS);

    PD_ALT2 = 0x03;
    PD_ALT1 = 0x00;
    PD_DDR  = 0xEB;
    PD_DR   = 0x00;

    UART_LCTL  |= LCTL_DLAB;
    UART_BRG_L  = (brg & (unsigned short)0x00FF);
    UART_BRG_H  = (brg & (unsigned short)0xFF00) >> (unsigned short)8;
    UART_LCTL  &= ~LCTL_DLAB;
    UART_FCTL   = 0x07;	// eZ80F91 date codes 0611 and after requires disabling FIFO.
    SetLCTL(UART_DATA_BITS, UART_PARITY, UART_STOP_BITS);
} /* end uart0_init */

