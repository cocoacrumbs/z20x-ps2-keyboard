# PS2KeyAdvanced ported to eZ80

This an early beta of a port of the Arduino PS2KeyAdvanced library from Paul 
Carpenter, PC Services to the eZ80 (more specifically, the Z20X computer).

The current status is that decoding the PS/2 key codes do work but the reverse
communication (eZ80 to PS/2 keyboard) does not yet work for some reason.

But, as it is now, it should already be useful to use a PS/2 keyboard with the
Z20X computer.

This is the kind of output that is being dumped to a terminal:

    Using Uart 0
    -----------------------------
    ZiLOG Developers Studio 5.3.4
    -----------------------------
    integer = 5
    decimal = 25
    float   = 1.260000
    eZ80F92 5 25 1.260000
    -----------------------------
    fc
    -----
    11e
    811e
    11e
    811e

``fc   ``indicates an error (since sending bytes to the keyboard doesn't work yet).

``11e  ``indicates the return key being pressed

``811e ``indicates the return key being released.

The text below is partially copied as is from the original README note of the 
Arduino PS2KeyAdvanced library by Paul Carpenter, PC Services.


### Introduction
After looking round for suitable libraries I found most were lacking in functionality and high in code and data footprint, so I created a series of PS2 Keyboard libraries. This is the second which fully supports the PS2 Keyboard Protocol, even allowing you control of keyboard LEDs (some have 4 LEDs) and changing settings..

The PS2 Keyboard interface is still needed for systems that have no USB and even if you have USB, you want it left for other uses.

The PS2 Keyboard interface is a Bi-directional two wire interface with a clock line and a data line which you connect to your Arduino (see above), the keyboard protocol has many nuances all of which are used in the other libraries of this series. this library allows you to access the keycodes sent from a keyboard into its small buffer and read out the codes with simple methods.

Returns any keypress as 16 bit integer, which includes a coded value for the key along with status for

   - Make/Break
   - CTRL, SHIFT, CAPS, ALT, GUI, ALT-GR Status
   - Alphanumeric/keyboard Function
   - 8 bit key code (defined in public header)

Fully featured PS2 keyboard library to provide

    - All keys have a keycode (ESC, A-Z and 0-9 as ASCII equivalents)
    - All function (F1 to F24), multimedia and movement keys supported
    - Parity checking of data sent/received
    - Resends data and requests resends when needed
    - Functions for get and/or set of
        - Scancode set in use
        - LED and LOCK control
        - ReadID
        - Reset keyboard
        - Send ECHO
    - Ignore Break codes for keys
    - Ignore typematic repeat of CTRL, SHIFT, ALT, Num, Scroll, Caps
    - Handles NUM, CAPS and SCROLL lock keys to LEDs
    - Handles NUM/SCROLL internally

### Contributor and Author Details
Author Paul Carpenter, PC Services

Web Site http://www.pcserviceselectronics.co.uk
