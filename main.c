#include <eZ80.h>
#include <stdio.h>

#include "setup-UART0.h"
#include "PS2KeyAdvanced.h"
#include "timer.h"

#ifdef _EZ80190
#define DEVICE_NAME          "eZ80190"
#define UZI_MODE_UART        (unsigned char)0x01
#endif
 
#ifdef _EZ80L92
#define DEVICE_NAME          "eZ80L92"
#endif
 
#ifdef _EZ80F93
#define DEVICE_NAME          "eZ80F93"
#endif
 
#ifdef _EZ80F92
#define DEVICE_NAME          "eZ80F92"
#endif
 
#ifdef _EZ80F91
#define DEVICE_NAME          "eZ80F91"
#endif
 

char device_name[] = DEVICE_NAME;


int main()
{
    int         ch      = 0;
    int         i       = 0;
    static char zds[]   = "ZiLOG Developers Studio 5.3.4";
    
    unsigned short  c = 0;

    printf("\nUsing Uart %i\n", 0);
    printf("-----------------------------\n");
    printf("%s\n", zds);
    printf("-----------------------------\n");
    printf("integer = %i\n", 5);
    printf("decimal = %d\n", 25);
    printf("float   = %f\n", 1.26);
    printf("%s %i %d %f\n", device_name, 5, 25, 1.26);
    printf("-----------------------------\n");


    // ps2_calibration();

    uart0_init();
    timer2_init(1); // 1 ms interval
    ps2_begin();
    ps2_echo();
    delayms(250);   // Originnaly 6 ms.
    c = ps2_read();
    printf("%4x\n", c);

    printf("-----\n");
    
    while (1)
    {
        c = ps2_read();
        if (c != 0)
        {
            printf("%4x\n", c);
            c = 0;
        }
    }

    return 0;
} /* end main */
